package tnelubimhar.com.kitchenwarrior;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    String[] categories = {
            "News","Bacon",
            "Beverages","Cake",
            "Breakfast","Candy",
            "Cocktails","Cookies",
            "Cupcakes","Dessert",
            "Homebrew","Pasta",
            "Pie","Pizza",
            "Salad","Sandwiches",
            "Snacks","Soups",
            "Vegetarian"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    public void onSectionAttached(int number) {

        mTitle = categories[number-1];//-- title of action bar
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */

        //--Variables
        ListView lvRecipes;
        //--
        private  String URL = "http://www.instructables.com/json-api/searchInstructables?type=id&locale=en_US&category=food&featured=true&offset=0&limit=60";
        String[] categories = {
                "News","Bacon",
                "Beverages","Cake",
                "Breakfast","Candy",
                "cocktails-and-mocktails","Cookies",
                "Cupcakes","Dessert",
                "Homebrew","Pasta",
                "Pie","Pizza",
                "Salad","Sandwiches",
                "snacks-and-appetizers","soups-and-stews",
                "vegetarian-and-vegan"
        };


        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }


        public PlaceholderFragment() {
        }


        //-- On create of fragment
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            //-- get view items
            lvRecipes = (ListView) rootView.findViewById(R.id.lvRecipes);//--get list view

            //-- get arguments of fragment
            Bundle bundle=getArguments();
            Integer currentCategoryNumber =bundle.getInt(ARG_SECTION_NUMBER);//-- get category number
            String currentCategory = categories[currentCategoryNumber-1];//-- current category

            //-- set category name to query string
            if (!currentCategory.equals("News")){
                URL = "http://www.instructables.com/json-api/searchInstructables?type=id&category=Food&offset=0&limit=18&locale=en_US&featured=true&channel="
                        + currentCategory.toLowerCase();
            }

            //-- get recipes from rest
            new FetchRecipes().execute(URL);


            return rootView;
        }


        //-- class for fetching data from rest
        class FetchRecipes extends AsyncTask<String, Void, String>
        {

            @Override
            protected String doInBackground(String... params)
            {
                return HttpManager.makeRequest(params[0]);//-- get raw json data
            }

            @Override
            protected void onPostExecute(String s)
            {
                super.onPostExecute(s);

                //-- recipe list for final usage
                final ArrayList<Recipes> recipesArrayList = new ArrayList<Recipes>();

                try
                {
                    JSONObject recipesJson = new JSONObject(s);//-- parse raw data to json
                    JSONArray jsonArray = recipesJson.getJSONArray("items");//-- get recipes array

                    //-- loop through array
                    for(int i = 0; i < jsonArray.length(); i ++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);//-- parse array item
                        Recipes recipes = new Recipes();//-- initialize new recipe
                        recipes.setId(jsonObject.getString("id"));//-- set id
                        recipes.setImage(jsonObject.getString("square3Url"));//-- set image
                        recipes.setTitle(jsonObject.getString("title"));//-- set title

                        recipesArrayList.add(recipes);//-- push to list

                    }
                }
                catch (JSONException e)
                {

                }

                //-- initialize array adapter
                final RecipesArrayAdapter adapter = new RecipesArrayAdapter(getActivity(), R.layout.recipe_item
                        ,recipesArrayList);



                lvRecipes.setAdapter(adapter);//-- set adapter of list view

                //-- list item on click to open details
                lvRecipes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Intent intent = new Intent(getActivity(), RecipeDetailActivity.class);//-- create new intent
                        intent.putExtra("RecipeId",recipesArrayList.get(position).getId().toString());//-- Put id as extra
                        intent.putExtra("RecipeTitle",recipesArrayList.get(position).getTitle());//-- put title as extra
                        startActivity(intent);//-- Start activity

                    }
                });

               // System.out.println(s);
            }
        }



        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

}
