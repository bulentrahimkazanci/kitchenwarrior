package tnelubimhar.com.kitchenwarrior;

/**
 * Created by Bulent on 28/04/15.
 */
public class Recipes {

    public String id;
    public String image;
    public String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
