package tnelubimhar.com.kitchenwarrior;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Bulent on 29/04/15.
 */
public class RecipeStepsArrayAdapter extends ArrayAdapter<RecipeSteps> {

    ArrayList<RecipeSteps> recipeStepList = new ArrayList<RecipeSteps>();//-- recipe steps list

    //-- Adapter constructor
    public RecipeStepsArrayAdapter(Context context, int resource, ArrayList<RecipeSteps> objects)
    {
        super(context, resource, objects);
        this.recipeStepList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;//-- define view holder

        if(getItemViewType(position) == 1)
        {
            if(convertView == null || !(convertView.getTag() instanceof ViewHolder))
            {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());//-- define inflater
                convertView = inflater.inflate(R.layout.recipe_step, null);//-- set layout and inflate

                holder = new ViewHolder();//-- initialize viewholder
                holder.image = (ImageView) convertView.findViewById(R.id.imgStep);//-- set image
                holder.body = (TextView) convertView.findViewById(R.id.tvStep);//-- set title

                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) convertView.getTag();
            }

            Picasso.with(parent.getContext()).load(recipeStepList.get(position).getImages()).into(holder.image);
            holder.body.setText(recipeStepList.get(position).getBody());
            return convertView;

        }
        else
        {
            if(convertView == null || !(convertView.getTag() instanceof ViewHolder))
            {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());//-- define inflater
                convertView = inflater.inflate(R.layout.recipe_step, null);//-- set layout and inflate

                holder = new ViewHolder();//-- initialize viewholder
                holder.image = (ImageView) convertView.findViewById(R.id.imgStep);//-- set image
                holder.body = (TextView) convertView.findViewById(R.id.tvStep);//-- set title

                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) convertView.getTag();
            }

            Picasso.with(parent.getContext()).load(recipeStepList.get(position).getImages()).into(holder.image);
            holder.body.setText(recipeStepList.get(position).getBody());
            return convertView;

        }

    }

    //-- view holder static class
    static class ViewHolder
    {
        ImageView image;
        TextView body;
    }
}
