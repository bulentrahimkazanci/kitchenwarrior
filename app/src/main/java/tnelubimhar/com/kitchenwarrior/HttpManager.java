package tnelubimhar.com.kitchenwarrior;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Bulent on 28/04/15.
 */
public class HttpManager
{

    public static String makeRequest(String getUrl)
    {
        HttpURLConnection urlConnection = null;//-- define connection
        BufferedReader reader = null; //-- buffer reader

        //-- raw json data holder
        String rawJson = null;

        try
        {
            URL url = new URL(getUrl);//-- create url
            urlConnection = (HttpURLConnection) url.openConnection();//--open connection
            urlConnection.setRequestMethod("GET");//-- set http method
            urlConnection.connect();//-- start connection

            InputStream inputStream = urlConnection.getInputStream();//-- get stream data
            StringBuffer buffer = new StringBuffer();//-- create buffer

            //-- check for stream data
            if (inputStream == null)
            {
                rawJson = null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;

            //-- read data
            while ((line = reader.readLine()) != null)
            {
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0)
            {
                rawJson = null;
            }

            rawJson = buffer.toString();//-- parse to string
        }
        catch (IOException e)
        {
            Log.e("HttpManager", "Error", e);
        }
        finally
        {
            //-- check for resources
            if (urlConnection != null)
            {
                urlConnection.disconnect();
            }
            if (reader != null)
            {
                try
                {
                    reader.close();
                }
                catch (IOException e)
                {
                    Log.e("HttpManager", "Error closing stream", e);
                }
            }
        }

        return rawJson;
    }
}