package tnelubimhar.com.kitchenwarrior;

/**
 * Created by Bulent on 29/04/15.
 */
public class RecipeSteps {

    //-- Properties
    public String id;
    public String body;
    public String image;

    //-- Constructor
    public RecipeSteps (){

    }

    //-- Getter and Setter


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getImages() {
        return image;
    }

    public void setImages(String image) {
        this.image = image;
    }
}
