package tnelubimhar.com.kitchenwarrior;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Bulent on 28/04/15.
 */
public class RecipesArrayAdapter extends ArrayAdapter<Recipes> {

    ArrayList<Recipes> recipeList = new ArrayList<Recipes>();//-- recipe list


    //-- adapter constructor
    public RecipesArrayAdapter(Context context, int resource, ArrayList<Recipes> objects)
    {
        super(context, resource, objects);
        this.recipeList = objects;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position)
    {
        if(position % 3  == 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder = null;//-- define view holder

        if(getItemViewType(position) == 1)
        {
            if(convertView == null || !(convertView.getTag() instanceof ViewHolder))
            {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());//-- define inflater
                convertView = inflater.inflate(R.layout.recipe_item, null);//-- set layout and inflate

                holder = new ViewHolder();//-- initialize viewholder
                holder.image = (ImageView) convertView.findViewById(R.id.recipeItemImage);//-- set image
                holder.title = (TextView) convertView.findViewById(R.id.recipeItemTitle);//-- set title

                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) convertView.getTag();
            }

            Picasso.with(parent.getContext()).load(recipeList.get(position).getImage()).into(holder.image);
            holder.title.setText(recipeList.get(position).getTitle());
            return convertView;

        }
        else
        {
            if(convertView == null || !(convertView.getTag() instanceof ViewHolder))
            {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());//-- define inflater
                convertView = inflater.inflate(R.layout.recipe_item, null);//-- set layout and inflate

                holder = new ViewHolder();//-- initialize viewholder
                holder.image = (ImageView) convertView.findViewById(R.id.recipeItemImage);//-- set image
                holder.title = (TextView) convertView.findViewById(R.id.recipeItemTitle);//-- set title

                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) convertView.getTag();
            }

            Picasso.with(parent.getContext()).load(recipeList.get(position).getImage()).into(holder.image);
            holder.title.setText(recipeList.get(position).getTitle());
            return convertView;

        }




    }

    //-- view holder static class
    static class ViewHolder
    {
        ImageView image;
        TextView title;
    }
}
