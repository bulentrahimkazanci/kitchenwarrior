package tnelubimhar.com.kitchenwarrior;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class RecipeDetailActivity extends ActionBarActivity {

    //-- Variables

    ListView lvSteps;
    TextView tvTitle;
    private String URL = "http://www.instructables.com/json-api/showInstructable?id=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_detail);

        //-- get id from intent
        Intent intent = getIntent();//-- get intent
        String recipeId = intent.getStringExtra("RecipeId");//-- get recipe id
        URL += recipeId;//-- add recipe id to query string

        //--get title from intent
        String actionBarTitle = intent.getStringExtra("RecipeTitle");
       //getActionBar().setTitle(actionBarTitle);//-- Change text in action bar
        setTitle(actionBarTitle);


        //-- set values of variables
        lvSteps = (ListView) findViewById(R.id.recipeDetailLv);


        //-- get recipe detail from rest
        new FetchRecipeDetail().execute(URL);


    }


    //-- class for fetching data from rest
    class FetchRecipeDetail extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... params)
        {
            return HttpManager.makeRequest(params[0]);//-- get raw json data
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);


            //-- recipe list for final usage
            final ArrayList<RecipeSteps> recipesArrayList = new ArrayList<RecipeSteps>();

            try
            {
                JSONObject recipeJson = new JSONObject(s);//-- parse raw data to json
                JSONArray jsonArray = recipeJson.getJSONArray("steps");//-- get recipes array

                //-- loop through array
                for(int i = 0; i < jsonArray.length(); i ++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);//-- parse array item
                    RecipeSteps recipes = new RecipeSteps();//-- initialize new recipe
                    recipes.setId(jsonObject.getString("id"));//-- set id

                    //-- replace tags in body and set
                    recipes.setBody(android.text.Html.fromHtml(jsonObject.getString("body")).toString());//-- set title

                    //-- cast image in json
                    JSONArray files = jsonObject.getJSONArray("files");
                    JSONObject file = files.getJSONObject(0);
                    recipes.setImages(file.getString("square3Url"));//-- set image

                    recipesArrayList.add(recipes);//-- push to list

                }
            }
            catch (JSONException e)
            {

            }

            //-- initialize array adapter
            final RecipeStepsArrayAdapter adapter = new RecipeStepsArrayAdapter(RecipeDetailActivity.this , R.layout.recipe_step
                    ,recipesArrayList);

            lvSteps.setAdapter(adapter);//-- set adapter of list view

        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_recipe_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
